/**
 * Author: Zarandi David (Azuwey)
 * Date: 02/24/2019
 * Source: HackerRank - https://www.hackerrank.com/challenges/lilys-homework/problem
 **/
#include <bits/stdc++.h>

using namespace std;

vector<string> split_string(string);

int lilysHomework(vector<int> arr) {
  int swap_c = 0;
  int as = arr.size();

  // Creating a "dictionary" from our input vector
  unordered_map<int, int> d = {};
  for (int i = 0; i < as; ++i) {
    d[arr[i]] = i;
  }

  // Sorting input vector
  vector<int> s = vector<int>(as);
  copy(arr.begin(), arr.end(), s.begin());
  sort(s.begin(), s.end());

  // Calculate swap count
  for (int i = 0; i < as; ++i) {
    if (arr[i] != s[i]) {
      ++swap_c;
      int ii = d[s[i]];
      d[arr[i]] = d[s[i]];
      arr[ii] = arr[i];
      arr[i] = s[i];
    }
  }

  return swap_c;
}

int main() {
  ofstream fout(getenv("OUTPUT_PATH"));

  int n;
  cin >> n;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');

  string arr_temp_temp;
  getline(cin, arr_temp_temp);

  vector<string> arr_temp = split_string(arr_temp_temp);

  vector<int> arr(n);

  for (int i = 0; i < n; ++i) {
    int arr_item = stoi(arr_temp[i]);

    arr[i] = arr_item;
  }

  int result_f = lilysHomework(arr);
  reverse(arr.begin(), arr.end());
  int result_b = lilysHomework(arr);

  fout << min(result_f, result_b) << "\n";

  fout.close();

  return 0;
}

vector<string> split_string(string input_string) {
  string::iterator new_end =
      unique(input_string.begin(), input_string.end(),
             [](const char &x, const char &y) { return x == y and x == ' '; });

  input_string.erase(new_end, input_string.end());

  while (input_string[input_string.length() - 1] == ' ') {
    input_string.pop_back();
  }

  vector<string> splits;
  char delimiter = ' ';

  size_t i = 0;
  size_t pos = input_string.find(delimiter);

  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));

    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }

  splits.push_back(
      input_string.substr(i, min(pos, input_string.length()) - i + 1));

  return splits;
}
