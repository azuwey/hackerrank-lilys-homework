# Insertion Sort Advanced Analysis

## Problem

Whenever George asks Lily to hang out, she's busy doing homework. George wants to help her finish it faster, but he's in over his head! Can you help George understand Lily's homework so she can hang out with him?

Consider an array of `n` distinct integers, `arr` = [a[0], a[1], ..., a[n - 1]] George can swap any two elements of the array any number of times. An array is beautiful if the sum of |arr[i] - arr[i - 1]| 0 < i < n is minimal.

Given the array `arr`, determine and return the minimum number of swaps that should be performed in order to make the array beautiful.

For example, `arr` = [7, 15, 12, 3]. One minimal array is [3, 7, 12, 15]. To get there, George performed the following swaps:

```s
Swap      Result
          [7, 15, 12, 3]
3 7       [3, 15, 12, 7]
7 15      [3, 7, 12, 15]
```

It took 2 swaps to make the array beautiful. This is minimal among the choice of beautiful arrays possible.

### Function Description

It should return an integer that represents the minimum number of swaps required.

`lilysHomework` has the following parameter(s):

- `arr`: an array of integers

### Input Format

The first line contains a single integer `n`, the number of elements in `arr`. The second line contains `n` space-separated integers arr[i].

The following `t` pairs of lines are as follows:

### Constraints

- 1 <= n <= 10^5
- 1 <= a[i] <= 2 * 10^9

### Output Format

Return the minimum number of swaps needed to make the array beautiful.

#### Sample Input

```s
4
2 5 3 1
```

#### Sample Output

```s
2
```

#### Explanation

Let's define array B = [1, 2, 3, 5] to be the beautiful reordering of `arr` The sum of the absolute values of differences between its adjacent elements is minimal among all permutations and only two swaps (1 with 2, and then 2 with 5) were performed.

## Source

[HackerRank - Lily's Homework](https://www.hackerrank.com/challenges/lilys-homework/problem)